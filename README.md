## Cara Nge-clone

1. Klik logo tulisan ***clone***. Lalu pilih clone pake **HTTPS**
2. Buka folder yang diinginkan di explorer, buka terminal di sana.
3. Ketik `git clone link_yang_sudah_dicopy`
4. Jika sudah, jalanin `composer install` dulu untuk download package penting Laravel
5. Setelah itu ubah `.env.example` jadi `.env`
6. Jalanin `php artisan key:generate`
