@extends('layouts.app')

@section('title', 'Bloom Production | Home')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
@endsection
@section('content')
    <div class="container hero-page d-flex flex-row justify-content-between">
        <div class="hero-content d-flex flex-column">
            <p class="fs-3 title-section" style="color:#c5c5c5">trendy collection</p>
            <div style="margin-top: -20px;">
                <h1 class=" display-1 title-section" style="font-size: 100px">fashion give</h1>
                <h1 class="display-1 title-section" style="color: var(---secondary-color); font-size: 100px; margin-top: -20px;">Impression</h1>
            </div>
            <p style="color:var(--font-black-color);width: max-content">Lorem ipsum dolor sit amet consectetur. Amet tellus
                sit dolor <br> pharetra. Accumsan eget condimentum non elit sagittis
                dictum <br> aliquet bibendum nullam ornare nullam.</p>
            <div class="btn-group-hero d-flex flex-row gap-2 mt-2">
                <p class="btn btn-explore text-white title-section fs-6">explore now</p>
                <p class="btn btn-collection  title-section fs-6">See collection</p>
            </div>
        </div>
        <div class="hero-img-group d-flex gap-4">
            <img src="{{asset('img/hero-1 (1).webp')}}"width="290">
            <img src="{{asset('img/hero-2.webp')}}" width="290">
        </div>
    </div>

    <div class="product-section container justify-content-between d-flex flex-row" style="background-color:var(--main-black-color); border-radius: 6px">
        <div class="group-product d-flex flex-row">
            <div><img src="{{asset('img/man.svg')}}"></div>
            <div class="align-self-center d-flex flex-column">
                <p class="fs-3 mb-0 text-white title-section mb-1" style="line-height: 1">man <br> collection's</p>
                <p class="text-white fs-6 mb-0"><u>See More</u></p>
            </div>
        </div>
        <hr style="background-color:#fff; color:white; rotate:90deg; width: 40px; height: 1px" class=" align-self-center m-0 bg-opacity-100">
        <div class="group-product d-flex flex-row">
            <div><img src="{{asset('img/woman.svg')}}"></div>
            <div class="align-self-center d-flex flex-column">
                <p class="fs-3 mb-0 text-white title-section mb-1" style="line-height: 1">woman <br> collection's</p>
                <p class="text-white fs-6 mb-0"><u>See More</u></p>
            </div>
            <hr style="background-color:#fff; color:white; z-index: 9999!important;" class="opacity-100 bg-opacity-100 d-flex position-relative">
        </div>
        <hr style="background-color:#fff; color:white; rotate:90deg; width: 40px; height: 1px" class=" align-self-center m-0 bg-opacity-100">
        <div class="group-product d-flex flex-row">
            <div><img src="{{asset('img/sport.svg')}}"></div>
            <div class="align-self-center d-flex flex-column">
                <p class="fs-3 mb-0 text-white mb-1 title-section" style="line-height: 1">man <br> collection's</p>
                <p class="text-white fs-6 mb-0"><u>See More</u></p>
            </div>
            <hr style="background-color:#fff; color:white; z-index: 9999!important;" class="opacity-100 bg-opacity-100 d-flex position-relative">
        </div>
    </div>

    <div class="deal-card-page container">
        <div class="title-header mb-5">
            <h1 class="display-2 title-section text-center">deal of the month</h1>
            <p class="text-center" style="color: var(--font-black-color)">Lorem ipsum dolor sit amet consectetur. Amet tellus sit dolor
                <br> pharetra. Accumsan eget condimentum non elit.</p>
        </div>
        <div class="card-wrapper  mt-5 row d-flex justify-content-center gap-5 row-cols-4">
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link" href="/detail-product">
                    <div class="card-img-top d-flex img-top-group"style="width:24px" >
                        <img src="{{asset('img/deal-1.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">nade</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>
                </a>


            </div>
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link" href="/detail-product">
                    <div class="card-img-top d-flex img-top-group"style="width:24px" >
                        <img src="{{asset('img/deal-2.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">nade</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>
                </a>

            </div>
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link" href="/detail-product">
                    <div class="card-img-top d-flex img-top-group"style="width:24px" >
                        <img src="{{asset('img/deal_3_fix.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">nade</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>
                </a>

            </div>
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a href="/detail-product" class="nav-link">
                    <div class="card-img-top d-flex img-top-group"style="width:240px" >
                        <img src="{{asset('img/deal_4_fix.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">nade</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>
                </a>

            </div>
        </div>
    </div>

    <div class="feature-page container">
        <div class="text-center feature-header">
            <h1 class="display-2 title-section">featured brand</h1>
            <p style="color: var(--font-black-color)">Lorem ipsum dolor sit amet consectetur. Amet tellus sit dolor <br> pharetra. Accumsan eget condimentum non elit.</p>
        </div>
        <div class="row row-cols-3 mt-4 d-flex justify-content-center">
            <img src="{{asset('img/sponsor1.svg')}}" class="mb-4">
            <img src="{{asset('img/sponsor2.svg')}}" class="mb-4">
            <img src="{{asset('img/sponsor3.svg')}}" class="mb-4">
            <img src="{{asset('img/sponsor4.svg')}}">
            <img src="{{asset('img/sponsor5.svg')}}">
            <img src="{{asset('img/sponsor6.svg')}}">
        </div>
    </div>


    <div class="collection-page">
        <div class="text-center feature-header">
            <h1 class="display-2 title-section">our collection</h1>
            <p style="color: var(--font-black-color)">Lorem ipsum dolor sit amet consectetur. Amet tellus sit dolor <br> pharetra. Accumsan eget condimentum non elit.</p>
        </div>
        <div class="card-wrapper px-5 mx-5  mt-5 row d-flex justify-content-center gap-5 row-cols-4">
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link" href="/detail-product">
                    <div class="card-img-top d-flex img-top-group"style="width:24px" >
                        <img src="{{asset('img/deal-1.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">nade</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>

                </a>

            </div>
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link"href="/detail-product">
                    <div class="card-img-top d-flex img-top-group"style="width:24px" >
                        <img src="{{asset('img/deal-2.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">nade</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>
                </a>
            </div>
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link"href="/detail-product">
                    <div class="card-img-top d-flex img-top-group"style="width:24px" >
                        <img src="{{asset('img/deal_3_fix.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">nade</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>
                </a>
            </div>
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link"href="/detail-product">
                    <div class="card-img-top d-flex img-top-group"style="width:240px" >
                        <img src="{{asset('img/deal_4_fix.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">nade</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>

                </a>
            </div>
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link"href="/detail-product">
                    <div class="card-img-top d-flex img-top-group"style="width:240px" >
                        <img src="{{asset('img/deal--5.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">third day</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>
                </a>
            </div>
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link"href="/detail-product">   <div class="card-img-top d-flex img-top-group"style="width:240px" >
                        <img src="{{asset('img/deal--6.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">hush puppies</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div></a>
            </div>
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link"href="/detail-product">
                    <div class="card-img-top d-flex img-top-group"style="width:240px" >
                        <img src="{{asset('img/deal--7.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">mango</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>
                </a>
            </div>
            <div class="card p-0 col" style="width: 240px; background:none;">
                <a class="nav-link"href="/detail-product">
                    <div class="card-img-top d-flex img-top-group"style="width:240px" >
                        <img src="{{asset('img/deal--8.svg')}}" width="240px" class="img-card">
                        <i class="fa-regular fa-heart fs-4 mt-3 d-flex justify-content-end" style="margin-left: -34px; z-index: 99"></i>
                    </div>
                    <div class="card-body p-0" style="">
                        <div class="body-card-top d-flex flex-row justify-content-between mt-2">
                            <div>
                                <p class="fs-4 m-0 title-section">vengoz</p>
                            </div>
                            <div class="align-self-center">
                                <p class="price-box m-0 title-section text-white fs-6 " style="background-color:var(--secondary-color);">IDR.120.000</p>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p style="color: var(--font-black-color); font-size: 13px; width: max-content; line-height: 1.4">Nade FTA38 VCS Ladies kemeja rayon
                                <br>tie dye marmer kerah piyama biru</p>
                        </div>

                    </div>
                </a>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <p class="btn text-white btn-view mt-5 text-center title-section" style="">view more</p>
        </div>
    </div>

    <div class="cta-page d-flex flex-row container"style="background-color:var(--main-black-color);">
        <div class="img-cta mt-3"><img src="{{asset('img/cta-img.webp')}}"></div>
        <div class="cta-content align-self-center">
            <h1 class="display-2 title-section text-white" style="line-height: 1.1;">
                <span style="color: var(--secondary-color)">flash sale </span> <br>
                discount up to 70%
            </h1>
            <p class="text-white">Lorem ipsum dolor sit amet consectetur. Amet tellus sit <br>
                dolor pharetra. Accumsan eget condimentum elit.
            </p>
            <button class="btn btn-collection title-section" style=" background-color:#fff;">See Collection</button>
        </div>
    </div>

    <footer class="footer-page d-flex flex-lg-row flex-column">
        <div class=" container justify-content-lg-center">
            <div class="footer-top d-flex flex-lg-row flex-column" style="gap: 200px">
                <div class="footer-abt">
                    <img src="{{asset('img/landing-logo.svg')}}">
                    <p class="footer-abt-desc mt-4" style="color: var(--font-black-color); width: max-content">
                        Lorem ipsum dolor sit amet consectetur. <br>Amet tellus sit dolor pharetra. Accumsan <br>eget condimentum elit.
                    </p>
                    <div class="footer-socmed col-2 ">
                        <div class="socmed d-flex flex-row gap-4">
                            <i class="fa-brands fa-facebook-f fs-4"></i>
                            <i class="fa-brands fa-whatsapp fs-4"></i>
                            <i class="fa-brands fa-instagram fs-4"></i>
                        </div>
                    </div>
                </div>
                <div class="footer-nav row d-lg-flex d-none " style="width: fit-content; gap: 90px">
                    <div class="abt-nav col-2 ">
                        <p class="footer-nav-title fs-3 title-section">About</p>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Our Company</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Trending Fashion</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Testimonial</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Website Policies</a>
                            </li>
                        </ul>
                    </div>
                    <div class="abt-nav col-2 ">
                        <p class="footer-nav-title fs-3 title-section">category</p>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Man Clothes</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Woman Clothes</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Sport Clothes</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Home & Lifestyle</a>
                            </li>
                        </ul>
                    </div>
                    <div class="abt-nav col-2 ">
                        <p class="footer-nav-title fs-3 title-section" style="width: max-content">Customer Care</p>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Contact Us</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Check Order</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color); width: max-content">Product Care & Repair</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">FAQ</a>
                            </li>
                        </ul>
                    </div>
                    <div class="abt-nav col-2 ">
                        <p class="footer-nav-title fs-3 title-section">legal area</p>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Term of Us</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Privacy Policy</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color)">Condition of Sale</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about" class="nav-link" style="color: var(--font-black-color); width: max-content">Bloom Production Career</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="footer-bottom mt-3 d-lg-flex d-none flex-column footer-grey text-center" style="font-size: 14px!important;">
                <hr class="">
                <div class="d-flex text-center justify-content-center">
                    <p class="text-center" style="color: var(--font-black-color)">Copyright © 2022 Bloom Production. All Right Reserved.</p>
                </div>
            </div>
        </div>
    </footer>



    <script src="https://kit.fontawesome.com/9e88c62f38.js" crossorigin="anonymous"></script>
@endsection
