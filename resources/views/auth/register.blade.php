@extends('layouts.auth')

@section('title', 'Bloom Production | Register')
@section('auth')
    <div class="text-center px-lg-5 mx-lg-2">
        <h1 class="auth-msg"><span class="text-orange">create</span> an account</h1>
        <p class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius velit pharetra amet consequat.</p>
        <form method="POST" action="{{ route('register') }}" class="px-3">
            @csrf
            <div class="bloom-form">
                <label for="username" class="text-start">Username</label>
                <input type="text" name="username" id="username" class="@error('username') input-invalid @enderror" placeholder="Enter your username" value="{{ old('username') }}">
                @error('username')
                <p class="text-start invalid-feedback d-block m-0">{{ $message }}</p>
                @enderror
            </div>
            <div class="bloom-form">
                <label for="email" class="text-start">Email Address</label>
                <input type="email" name="email" id="email" class="@error('username') input-invalid @enderror" placeholder="Enter your email" value="{{ old('email') }}">
                @error('email')
                <p class="text-start invalid-feedback d-block m-0">{{ $message }}</p>
                @enderror
            </div>
            <div class="bloom-form">
                <label for="password" class="text-start">Password</label>
                <input type="password" name="password" id="password" class="@error('username') input-invalid @enderror" placeholder="Enter your password">
                @error('password')
                <p class="text-start invalid-feedback d-block m-0">{{ $message }}</p>
                @enderror
            </div>
            <button type="submit" class="btn-bloom my-4">register account</button>
            <p>Already have an account? <a href="/login" class="text-orange">Login account</a></p>
        </form>
    </div>

@endsection
