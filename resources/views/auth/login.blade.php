@extends('layouts.auth')

@section('title', 'Bloom Production | Login')
@section('auth')
    <div class="text-center px-lg-5 mx-lg-2">
        <h1 class="auth-msg"><span class="text-orange">welcome</span> back</h1>
        <p class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius velit pharetra amet consequat.</p>
        <form method="POST" action="{{ route('login') }}" class="px-3">
            @csrf
            <div class="bloom-form">
                <label for="email" class="text-start">Email Address</label>
                <input type="text" name="email" id="email" class="@error('email') input-invalid @enderror" placeholder="Enter your email" value="{{ old('email') }}">
                @error('email')
                <p class="text-start invalid-feedback d-block m-0">{{ $message }}</p>
                @enderror
            </div>
            <div class="bloom-form">
                <label for="password" class="text-start">Password</label>
                <input type="password" name="password" id="password" class="@error('email') input-invalid @enderror" placeholder="Enter your password">
                @error('password')
                <p class="text-start invalid-feedback d-block m-0">{{ $message }}</p>
                @enderror
            </div>
            <div class="d-flex justify-content-between">
                <label for="check" class="d-block text-start">
                    <input type="checkbox" id="check" class="me-1">
                    Remember Me
                </label>
                <a href="#" class="text-secondary">Forgot Password?</a>
            </div>
            <button type="submit" class="btn-bloom my-4">Login account</button>
            <p>Don't have an account? <a href="/register" class="text-orange">Register for free!</a></p>
        </form>
    </div>

@endsection
