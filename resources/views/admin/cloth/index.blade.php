@extends('adminlte::page')

@section('title', 'Dashboard | Cloth Edit')

@section('content_header')
    {{-- <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard Posts</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
        </div>
        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar" class="align-text-bottom"></span>
            This week
        </button>
        </div>
    </div> --}}
    <h1>Cloth List</h1>
@stop

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block" role="alert">
            <strong>{{$message}}</strong>
        </div>

        <img src="{{ asset('images/'.Session::get('image')) }}" />
    @endif
    <table id="table_id" class="dataTable table table-bordered">
        <thead>
        <tr>
            <th>No.</th>
            <th>Cloth Name</th>
            <th>Stock Avaible</th>
            <th>Price</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($cloths as $cloth)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $cloth->name }}</td>
                    <td>{{ $cloth->stock }}</td>
                    <td>{{ $cloth->price }}</td>
                    <td>{{ Str::limit($cloth->description) }}</td>
                    <td class="d-flex">
                        <a href="/dashboard/cloth/{{ $cloth->id }}" class="btn btn-dark h-100">Show</a>
                        <a href="/dashboard/cloth/{{ $cloth->id }}/edit" class="btn btn-warning h-100 mx-1">Edit</a>
                        <form action="{{ route('cloth.destroy', $cloth->id) }}" method="POST" class="d-inline-block">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                Delete
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
        </tbody>
    </table>

    <p>Page for change Cloth.</p>
    <div class="container">
        <div class="row">
            <div class="col-md-4 p-1 mb-3">
                <div class="card">
                    <img src="https://source.unsplash.com/800x400?" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Card title</h5>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <img src="https://source.unsplash.com/800x400?" class="card-img-top" alt="...">
                <div class="card-body">
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.13.1/datatables.min.css"/>
@stop

@section('js')
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.13.1/datatables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table_id').DataTable();
        });
        const alertList = document.querySelectorAll('.alert')
        const alerts = [...alertList].map(element => new bootstrap.Alert(element))
    </script>
@stop
