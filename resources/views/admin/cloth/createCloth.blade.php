@extends('adminlte::page')

@section('title', 'Dashboard | Create Cloth')

@section('content_header')
    <h1>Add New Cloth</h1>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8">
        <form action="{{ route('cloth.store') }}" method="POST" class="d-inline" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="image">Image Cloth : </label>
                {{-- <img src="{{ $cloth->image }}" id="image" name="image" alt="image" width="400" class="d-block mb-3"> --}}
                <label class="block mb-4">
                    <span class="sr-only">Choose File</span>
                    <input type="file" name="image"
                        class="custom-file block w-full text-sm text-gray-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-blue-50 file:text-blue-700 hover:file:bg-blue-100" />
                    @error('image')
                        <p class="invalid-feedback">{{ $message }}</p>
                    @enderror
                </label>
            </div>
            <div class="mb-3">
                <label for="name">Category Cloth : </label>
                <select class="form-control" id="exampleFormControlSelect1" name="category_id">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">
                                {{ $category->name }}
                        </option>
                    @endforeach
                  </select>
            </div>
            <div class="mb-3">
                <label for="name">Name Cloth : </label>
                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"></input>
            </div>
            <div class="mb-3">
                <label for="stock">Stock Cloth : </label>
                <input type="text" class="form-control" id="stock" name="stock" value="{{ old('stock') }}"></input>
            </div>
            <div class="mb-3">
                <label for="price">Price Cloth : </label>
                <input type="text" class="form-control" id="price" name="price" value="{{ old('price') }}"></input>
            </div>
            <div class="mb-3">
                <label for="description">Description Cloth : </label>
                <textarea id="description" name="description" rows="15" cols="88">
                    {{ old('description') }}
                </textarea>

            </div>
            <a href="/dashboard/cloth" class="btn btn-dark mb-4">Back</a>

            <button type="submit" class="btn btn-warning mb-4">Add new Cloth</button>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.13.1/datatables.min.css"/>
@stop

@section('js')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.13.1/datatables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table_id').DataTable();
        });
    </script>
@stop
