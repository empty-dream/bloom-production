@extends('adminlte::page')

@section('title', 'Dashboard | Cloth Edit')

@section('content_header')
    <h1>Show Cloth</h1>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <div class="mb-3">
                <label for="name">Image Cloth : </label>
                <img src="{{ asset('storage/'.$cloth->image) }}" id="image" name="image" alt="image" width="400" class="d-block">
            </div>
            <div class="mb-3">
                <label for="name">Category Cloth : </label>
                <p>{{ $cloth->category_id }}</p>
            </div>
            <div class="mb-3">
                <label for="name">Name Cloth : </label>
                <p>{{ $cloth->name }}</p>
            </div>
            <div class="mb-3">
                <label for="name">Stock Cloth : </label>
                <p>{{ $cloth->stock }}</p>
            </div>
            <div class="mb-3">
                <label for="name">Price Cloth : </label>
                <p>{{ $cloth->price }}</p>
            </div>
            <div class="mb-3">
                <label for="name">Description Cloth : </label>
                <p>{{ $cloth->description }}</p>
            </div>
            <a href="/dashboard/cloth" class="btn btn-dark mb-4">Back</a>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop
