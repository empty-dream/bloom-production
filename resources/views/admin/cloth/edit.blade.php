@extends('adminlte::page')

@section('title', 'Dashboard | Cloth Edit')

@section('content_header')
    <h1>Edit Cloth</h1>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8">
        <form action="{{ route('cloth.update', $cloth) }}" method="POST" class="d-inline" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="image">Image Cloth : </label>
                <img src="{{ $cloth->image }}" id="image" name="image" alt="image" width="400" class="d-block mb-3">
                <label class="block mb-4">
                    <span class="sr-only">Choose File</span>
                    <input type="file" name="image"
                        class="custom-file block w-full text-sm text-gray-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-blue-50 file:text-blue-700 hover:file:bg-blue-100" />
                    @error('image')
                        <p class="invalid-feedback">{{ $message }}</p>
                    @enderror
                </label>
            </div>
            <div class="mb-3">
                <label for="name">Category Cloth : </label>
                <select class="form-control" id="exampleFormControlSelect1" name="category_id">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">
                                {{ $category->name }}
                        </option>
                    @endforeach
                  </select>
            </div>
            <div class="mb-3">
                <label for="name">Name Cloth : </label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $cloth->name }}"></input>
            </div>
            <div class="mb-3">
                <label for="stock">Stock Cloth : </label>
                <input type="text" class="form-control" id="stock" name="stock" value="{{ $cloth->stock }}"></input>
            </div>
            <div class="mb-3">
                <label for="price">Price Cloth : </label>
                <input type="text" class="form-control" id="price" name="price" value="{{ $cloth->price }}"></input>
            </div>
            <div class="mb-3">
                <label for="description">Description Cloth : </label>
                <textarea id="description" name="description" rows="15" cols="88">
                    {{ $cloth->description }}
                </textarea>

            </div>
            <a href="/dashboard/cloth" class="btn btn-dark mb-4">Back</a>

            <button type="submit" class="btn btn-warning mb-4">Update</button>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop
