@extends('adminlte::page')

@section('title', 'Dashboard | Cloth Edit')

@section('content_header')
    <h1>Cloth Trash</h1>
@stop

@section('content')
    <div class="container">
        {{-- @if(session()->has('restore-success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Great!</strong> {{ session('restore-success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif(session()->has('delete-success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Great!</strong> {{ session('delete-success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif --}}
        <table id="table_id" class="dataTable table table-bordered">
            <thead>
            <tr>
                <th>No.</th>
                <th>Cloth Name</th>
                <th>Cloth Stock</th>
                <th>Cloth Price</th>
                <th>Cloth Dsscription</th>
                <th>Deleted At</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($cloths as $cloth)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $cloth->name }}</td>
                    <td>{{ $cloth->stock }}</td>
                    <td>{{ $cloth->price }}</td>
                    <td>{{ $cloth->stock }}</td>
                    <td>{{ Str::limit($cloth->description) }}</td>
                    <td>{{ $cloth->deleted_at->diffForHumans() }}</td>
                    <td class="d-flex">
                        <form action="{{ route('cloth-trash.restore', $cloth->id) }}" method="POST" class="d-inline-block mx-3">
                            @csrf
                            <button type="submit" class="btn btn-success">
                                Restore
                            </button>
                        </form>
                        <form action="{{ route('cloth-trash.delete', $cloth->id) }}" method="POST" class="d-inline-block mx-3" onsubmit="return confirm('This action will permanently delete this category. Do you wish to continue?')">
                            @csrf
                            <button type="submit" class="btn btn-danger">
                                Delete
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.13.1/datatables.min.css"/>
@stop

@section('js')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.13.1/datatables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table_id').DataTable();
        });
    </script>
@stop
