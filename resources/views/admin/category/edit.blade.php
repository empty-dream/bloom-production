@extends('adminlte::page')

@section('title', 'Dashboard | Create Category')

@section('content_header')
    <h1>Edit Category</h1>
@stop

@section('content')
    <div class="container">
        <form action="{{ route('category.update', $category->id) }}" method="post">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="name" class="form-label">Category Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Category name here..." value="{{ $category->name }}">
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@stop

@section('css')
@stop

@section('js')
@stop
