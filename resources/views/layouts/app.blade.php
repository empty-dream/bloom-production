<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/main-style.css')}}">

    @yield('css')
    <style>
        .offcanvas-togler {
            border: 0;
            background: transparent;
            margin: 0;
            padding: 0;
        }
        button.btn-close:focus {
            box-shadow: none;
            border: none;
        }
        .offcanvas-list-item {
            font-family: 'Bebas Neue';
            list-style-type: none;
            text-align: center;
        }
        .offcanvas-list-item a {
            text-decoration: none;
            color: #333;
            font-size: 2rem;
        }
        .offcanvas-list-item:hover a {
            color: #222;
        }
    </style>

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
    <main class="py-4">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <div class="navbar-brand">
                    <img src="{{ asset('img/landing-logo.svg') }}" width="44">
                </div>

                <div class="navbar-collapse justify-content-center ">
                    <ul class="navbar-nav  title-section text-center gap-3">
                        <li class="nav-item">
                            <a class="nav-link active fs-5">Men</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fs-5" href="/woman">Woman</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fs-5">Sports</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fs-5">Home and Lifestyle</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fs-5">About</a>
                        </li>
                    </ul>
                </div>
                <div class="navbar-icon-group d-flex gap-5">
                    <img src="{{asset('img/cart-nav.svg')}}">
                    <button type="button" class="offcanvas-togler" data-bs-toggle="offcanvas" data-bs-target="#offcanvas" aria-controls="offcanvas">
                        <img src="{{asset('img/nav-profile.svg')}}">
                    </button>
                </div>
            </div>
        </nav>
        @yield('content')
        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvas" aria-labelledby="offcanvasExampleLabel">
            <div class="offcanvas-header">
                <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <ul class="offcanvas-list h-100">
                    <li class="offcanvas-list-item"><a href="#">home</a></li>
                    @auth
                        <li class="offcanvas-list-item"><a href="#">cart</a></li>
                        <li class="offcanvas-list-item"><a href="#">logout</a></li>
                    @endauth
                    @guest
                        <li class="offcanvas-list-item"><a href="/login">login</a></li>
                    @endguest
                </ul>
            </div>
        </div>
    </main>

@yield('js')
</body>
</html>
