<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/main-style.css')}}">
    <style>
        * {
            font-family: 'Arial', sans-serif;
            color: var(--main-black-color);
        }
        .auth-img {
            background-image: url("@if(\Illuminate\Support\Facades\Request::url() === route('login')) {{ asset('img/login-img.png') }} @elseif(\Illuminate\Support\Facades\Request::url() === route('register')) {{ asset('img/register-img.jpg') }} @endif");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
<div class="container-fluid">
    <div class=" row">
        <div class="col-lg-6 col-12 h-full">
            <div class="container p-3">
                <img src="{{ asset('img/landing-logo.svg') }}" alt="Bloom Production Logo">
                <div class="d-flex flex-column align-items-center">
                    @yield('auth')
                </div>
            </div>
        </div>
        <div class="col-lg-6 d-lg-block d-none auth-img">
        </div>
    </div>

</div>
</body>
</html>
