<?php

namespace App\Http\Controllers;
use \App\Models\Cloth;

use Illuminate\Http\Request;

class ClothTrashController extends Controller
{
    public function index() {
        return view('admin.cloth.trashCloth', [
            'cloths' => Cloth::onlyTrashed()->get()
        ]);
    }
    public function restore($id) {
        $cloth = Cloth::withTrashed()->where('id', $id);
        $cloth->restore();

        return redirect()->route('cloth-trash.index')->with('restore-success', 'Selected cloth has been restored');
    }
    public function destroy($id) {
        $cloth = Cloth::withTrashed()->where('id', $id);
        $cloth->forceDelete();

        return redirect()->route('cloth-trash.index')->with('delete-success', 'Selected cloth has been permanently deleted');
    }
}
