<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryTrashController extends Controller
{
    public function index() {
        return view('admin.category.trash', [
            'categories' => Category::onlyTrashed()->get()
        ]);
    }
    public function restore($id) {
        $category = Category::withTrashed()->where('id', $id);
        $category->restore();

        return redirect()->route('category-trash.index')->with('restore-success', 'Selected category has been restored');
    }
    public function destroy($id) {
        $category = Category::withTrashed()->where('id', $id);
        $category->forceDelete();

        return redirect()->route('category-trash.index')->with('delete-success', 'Selected category has been permanently deleted');
    }
}
