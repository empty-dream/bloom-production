<?php

namespace App\Http\Controllers;

use App\Models\Cloth;
use App\Models\Category;
use Illuminate\Http\Request;

class DashboardClothController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.cloth.index', [
            'cloths' => Cloth::latest()->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cloth.createCloth', [
            'categories' => Category::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cloth = new Cloth();
        $validatedData = $request->validate([
            'name' => 'required|max:255|string',
            'stock' => 'required|integer',
            'price' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:png,jpg,jpeg,gif,svg,webp|max:2048',
            'category_id' => 'required',
        ]);
        $image_path = $request->file('image')->store('image', 'public');

        $cloth->name = $validatedData['name'];
        $cloth->stock = $validatedData['stock'];
        $cloth->price = $validatedData['price'];
        $cloth->description = $validatedData['description'];
        $cloth->image = $image_path;
        $cloth->category_id = $validatedData['category_id'];
        $cloth->save();
        return redirect()->route('cloth.index')->with('create-success', 'New category has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cloth  $cloth
     * @return \Illuminate\Http\Response
     */
    public function show(Cloth $cloth)
    {
        return view('admin.cloth.show', [
            'cloth' => $cloth,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cloth  $cloth
     * @return \Illuminate\Http\Response
     */
    public function edit(Cloth $cloth)
    {
        return view('admin.cloth.edit', [
            'cloth' => $cloth,
            'categories' => Category::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cloth  $cloth
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cloth $cloth)
    {
        $cloth = Cloth::find($cloth->id);
        $validatedData = $request->validate([
            'name' => 'required|max:255|string',
            'stock' => 'required|integer',
            'price' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:png,jpg,jpeg,gif,svg,webp|max:2048',
            'category_id' => 'required',
        ]);

        // $imageName = time().'.'.$request->image->extension();
        // Public Folder
        $image_path = $request->file('image')->store('image', 'public');

        $cloth->name = $validatedData['name'];
        $cloth->stock = $validatedData['stock'];
        $cloth->price = $validatedData['price'];
        $cloth->description = $validatedData['description'];
        $cloth->image = $image_path;
        $cloth->category_id = $validatedData['category_id'];
        $cloth->save();

        return redirect('/dashboard/cloth')
            ->with('cloth-update','You have successfully edit data cloth.');



        // $category->name = $validatedData['name'];
        // $category->update();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cloth  $cloth
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cloth $cloth)
    {
        $cloth = Cloth::find($cloth->id);
        $cloth->delete();

        return redirect()->route('cloth.index')->with('delete-success', 'Selected cloth has been moved to trash');
    }
}
