<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cloth>
 */
class ClothFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->words(4, true),
            'stock' => $this->faker->randomNumber(5),
            'price' => $this->faker->randomFloat(2),
            'description' => $this->faker->paragraphs(4, true),
            'category_id' => mt_rand(1,20),
        ];
    }
}
