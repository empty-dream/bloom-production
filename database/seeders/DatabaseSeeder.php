<?php

namespace Database\Seeders;
use App\Models\Cloth;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
         \App\Models\User::factory(10)->create();

         \App\Models\User::factory()->create([
             'username' => 'Admin',
             'email' => 'admin@example.com',
             'password' => Hash::make('admin12345'),
             'is_admin' => true
         ]);
        foreach(range(1, 20) as $index) {
            \App\Models\Category::factory()->create([
                'name' => $faker->firstName()
            ]);
        }

        Cloth::factory(100)->create();
    }
}
