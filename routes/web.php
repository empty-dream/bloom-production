<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use \App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClothTrashController;
use \App\Http\Controllers\CategoryTrashController;
use App\Http\Controllers\DashboardClothController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Route::get('/woman', function () {
    return view('landing-woman');
});
Route::get('/detail-product', function (){
    return view('product-detail');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::middleware(['auth', 'admin'])->group(function() {
    Route::get('/dashboard', function(){
        return view('admin.index');
    });
    Route::resource('dashboard/cloth', DashboardClothController::class);
    Route::controller(ClothTrashController::class)->group(function () {
        Route::get('/dashboard/trash/cloth', 'index')->name('cloth-trash.index');
        Route::post('/dashboard/trash/cloth/{cloth}', 'restore')->name('cloth-trash.restore');
        Route::post('/dashboard/trash/cloth/{cloth}/delete', 'destroy')->name('cloth-trash.delete');
    });
    Route::resource('dashboard/category', CategoryController::class);
    Route::controller(CategoryTrashController::class)->group(function () {
        Route::get('/dashboard/trash/category', 'index')->name('category-trash.index');
        Route::post('/dashboard/trash/category/{category}', 'restore')->name('category-trash.restore');
        Route::post('/dashboard/trash/category/{category}/delete', 'destroy')->name('category-trash.delete');
    });
});
